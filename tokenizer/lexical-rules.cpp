#include "lexical-rules.h"

LexicalRules::LexicalRules(const string &rules_file, const string &input_file) {
    this->sm = generateStateMachine(rules_file);
    this->input = FileReader(input_file);
    this->available_tokens = 0;
}

#include <iostream>

vector<Token> LexicalRules::tokens() {
    auto v = vector<Token>();

    auto t = nextToken();
    while (!(t.name.empty() && !isError())) {
        if (t.name.empty()) {
            cerr << "Lexical warning: undefined token " << errorLocation(t.lexeme)[0] << ':'
                 << errorLocation(t.lexeme)[1] << '\t' << t.lexeme << endl;
            t.name = "undefined";
        }
        v.push_back(t);
        t = nextToken();
    }

    return v;
}

Token LexicalRules::nextToken() {
    while (!available_tokens) {
        char c = input.getChar();
        if (!c) return Token("", {}, true);

        available_tokens = sm.readCharacter(c);
    }

    auto token = sm.getOutput();
    if (token.name.empty())
        error_flag = true;

    available_tokens--;
    return token;
}

bool LexicalRules::isError() {
    if (error_flag) {
        error_flag = false;
        return true;
    }
    return false;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-narrowing-conversions"

vector<int> LexicalRules::errorLocation(string &false_lexeme) {
    auto location = input.charLocation();
    location[1] -= (false_lexeme.size() + 1);
    return location;
}

#pragma clang diagnostic pop

//TODO: To be implemented.
StateMachine LexicalRules::generateStateMachine(const string &rules_file) {

    ///PUNCTUATIONS
    regex keyword_punc_reg1(";");
    Token punc_1(";", {keyword_punc_reg1}, true);

    regex keyword_punc_reg2(",");
    Token punc_2(",", {keyword_punc_reg2}, true);

    regex keyword_punc_reg3("\\(");
    Token punc_3("(", {keyword_punc_reg3}, true);

    regex keyword_punc_reg4("\\)");
    Token punc_4(")", {keyword_punc_reg4}, true);

    regex keyword_punc_reg5("\\{");
    Token punc_5("{", {keyword_punc_reg5}, true);

    regex keyword_punc_reg6("\\}");
    Token punc_6("}", {keyword_punc_reg6}, true);

    vector<Token> punctuations = {punc_1, punc_2, punc_3, punc_4, punc_5, punc_6};

    ///KEYWORDS
    regex keyword_reg1("boolean");
    Token boolean_KW("boolean", {keyword_reg1}, true);

    regex keyword_reg2("float");
    Token float_KW("float", {keyword_reg2}, true);

    regex keyword_reg3("int");
    Token int_KW("int", {keyword_reg3}, true);

    regex keyword_reg4("if");
    Token if_KW("if", {keyword_reg4}, true);

    regex keyword_reg5("else");
    Token else_KW("else", {keyword_reg5}, true);

    regex keyword_reg6("while");
    Token while_KW("while", {keyword_reg6}, true);

    vector<Token> keywords = {boolean_KW, float_KW, int_KW, if_KW, else_KW, while_KW};

    ///DEFINITIONS
    regex id_reg("([a-z]|[A-Z])(([a-z]|[A-Z])|[0-9])*");
    Token id("id", {id_reg}, false);

    //Separate definition regex.
    regex num_reg1("[0-9]+");
    regex num_reg2("[0-9]+\\.[0-9]+(E[0-9]+)?");
    Token num("num", {num_reg1, num_reg2}, false);

    regex relop_reg1("==");
    regex relop_reg2("!=");
    regex relop_reg3(">");
    regex relop_reg4(">=");
    regex relop_reg5("<");
    regex relop_reg6("<=");
    Token relop("relop", {relop_reg1, relop_reg2, relop_reg3, relop_reg4, relop_reg5, relop_reg6}, false);

    regex assign_reg("=");
    Token assign("assign", {assign_reg}, false);

    regex addop_reg1("\\+");
    regex addop_reg2("-");
    Token addop("addop", {addop_reg1, addop_reg2}, false);

    regex mulop_reg1("\\*");
    regex mulop_reg2("\\/");
    Token mulop("mulop", {mulop_reg1, mulop_reg2}, false);

    vector<Token> definitions = {id, num, relop, assign, addop, mulop};

    return StateMachine(punctuations, keywords, definitions);
}
