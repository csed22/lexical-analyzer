#include <fstream>
#include "file-reader.h"

#pragma ide diagnostic ignored "cppcoreguidelines-pro-type-member-init"

FileReader::FileReader(const string &file_path) {
    this->file = loadFile(file_path);
    this->i = 0;
    this->row = 1;
    this->col = 1;
}

char FileReader::getChar() {
    if (i < file.size()) {
        char c = file[i];
        i++, col++;
        if (c == '\n') {
            row++;
            col = 1;
        }
        return c;
    }
    return '\0';
}

vector<int> FileReader::charLocation() {
    return {row, col};
}

string FileReader::loadFile(const string &file_path) {
    ifstream fs(file_path);
    string str((istreambuf_iterator<char>(fs)),
               istreambuf_iterator<char>());
    fs.close();
    return str.substr(0, str.length() - 1);
}

FileReader::FileReader() = default;
