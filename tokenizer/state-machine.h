#ifndef PHASE1_LEXICAL_STATE_MACHINE_H
#define PHASE1_LEXICAL_STATE_MACHINE_H

#include "token.h"

class StateMachine {
private:
    vector<Token> rules[3];
    vector<Token> acceptedStates;

    void resetTokens();

    bool prev_accepted = false;
    bool double_output = false;
    Token punctuation_token = Token("", {}, true);

    vector<Token> separateTailingPunctuation();

public:
    StateMachine();

    explicit StateMachine(vector<Token> &punctuations, vector<Token> &keywords, vector<Token> &definitions);

    int readCharacter(char input);

    Token getOutput();

};

#endif //PHASE1_LEXICAL_STATE_MACHINE_H
