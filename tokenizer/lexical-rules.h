#ifndef PHASE1_LEXICAL_LEXICAL_RULES_H
#define PHASE1_LEXICAL_LEXICAL_RULES_H

#include "file-reader.h"
#include "state-machine.h"

class LexicalRules {
private:
    StateMachine sm;
    FileReader input;
    int available_tokens;
    bool error_flag = false;

    static StateMachine generateStateMachine(const string &rules_file);

    Token nextToken();

    bool isError();

    vector<int> errorLocation(string &false_lexeme);

public:

    explicit LexicalRules(const string &rules_file, const string &input_file);

    // To separate the lexical rules logic from main program (might be reverted to reduce the number of passes)
    vector<Token> tokens();
};

#endif //PHASE1_LEXICAL_LEXICAL_RULES_H
