#include "token.h"

#pragma ide diagnostic ignored "performance-unnecessary-value-param"

Token::Token(string name, vector<regex> patterns, bool limited_size) {
    this->name = name;
    this->lexeme = "";

    this->matched = false;
    this->patterns = patterns;

    if (limited_size)
        this->max_size = name.size();
    else
        this->max_size = -1;
}

#define PARTIAL_EARLY_MATCH CheckLexeme('\0').back() != lexeme.back()
#define PARTIAL_LATE_MATCH CheckLexeme('\0').front() != lexeme.front()
#define MAX_SIZE_EXCEEDED (max_size >= 0 && lexeme.size() > max_size)

MatchState Token::checkChar(char c) {
    matched = false;
    auto prev_lexeme = CheckLexeme('\0');
    if (!CheckLexeme(c).empty()) {
        if (PARTIAL_EARLY_MATCH || PARTIAL_LATE_MATCH || MAX_SIZE_EXCEEDED)
            return TRAP;
        matched = true;
        return MATCH;
    }
    return PENDING;
}

bool Token::isMatched() const {
    return matched;
}

void Token::clearLexeme() {
    lexeme = "";
}

void Token::truncateLexeme(int trunc) {
    if (lexeme.size() > trunc)
        lexeme = lexeme.substr(0, lexeme.size() - trunc);
}

string longestString(vector<string> strings);

string Token::CheckLexeme(char c) {
    vector<string> matches;
    smatch m;
    if (c)
        lexeme = lexeme + c;
    for (auto &pattern : patterns) {
        if (regex_search(lexeme, m, pattern))
            matches.push_back(m[0]);
    }
    if (matches.empty())
        return "";
    return longestString(matches);
}

string longestString(vector<string> strings) {
    string str;
    for (auto &s : strings)
        if (s.size() > str.size())
            str = s;
    return str;
}
