#ifndef PHASE1_LEXICAL_TOKEN_H
#define PHASE1_LEXICAL_TOKEN_H

#include <regex>

#define size_type unsigned long long

using namespace std;

enum MatchState {
    TRAP = 0, PENDING = 1, MATCH = 2
};

class Token {
private:
    bool matched;
    size_type max_size;
    vector<regex> patterns;

    string CheckLexeme(char c);

public:
    string name;
    string lexeme;

    Token(string name, vector<regex> patterns, bool limited_size);

    MatchState checkChar(char c);

    bool isMatched() const;

    void clearLexeme();

    void truncateLexeme(int trunc);
};

#undef size_type
#endif //PHASE1_LEXICAL_TOKEN_H
