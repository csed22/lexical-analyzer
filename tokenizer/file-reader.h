#ifndef PHASE1_LEXICAL_FILE_READER_H
#define PHASE1_LEXICAL_FILE_READER_H

#include <string>
#include <vector>

#define size_type unsigned long long

using namespace std;

class FileReader {
private:
    string file;
    size_type i;
    int row, col;

    static string loadFile(const string &file_path);

public:
    FileReader();

    explicit FileReader(const string &file_path);

    char getChar();

    vector<int> charLocation();
};

#undef size_type
#endif //PHASE1_LEXICAL_FILE_READER_H
