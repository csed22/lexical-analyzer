#include <iostream>
#include "tokenizer/lexical-rules.h"

int main() {
    LexicalRules lex("rules.txt", "input.txt");

    auto tokens = lex.tokens();
    for (auto &t : tokens)
        cout << t.name << endl;

    return system("pause");
}
